import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

import Home             from '@/views/Home.vue'
import Login            from '@/views/usuario/Login.vue'

// CATALOGOS
import Niveles        from '@/views/catalogos/Niveles.vue'
import Usuarios       from '@/views/catalogos/Usuarios.vue'
import Terapeutas     from '@/views/catalogos/Terapeutas.vue'
import Pacientes      from '@/views/catalogos/Pacientes.vue'
import Prospectos     from '@/views/catalogos/Prospectos.vue'
import Coordinaciones from '@/views/catalogos/Coordinaciones.vue'
import Servicios      from '@/views/catalogos/Servicios.vue'

// ADMINISTRACION
import ConfigHorario from '@/views/terapeuta/ConfigHorario.vue'


// ESCUELA
import Maestros from '@/views/escuela/Maestros.vue'
import Tutores  from '@/views/escuela/Tutores.vue'
import Alumnos  from '@/views/escuela/Alumnos.vue'
import Grupos   from '@/views/escuela/Grupos.vue'

// AGENDA
import Agenda     from '@/views/Terapias/Agenda.vue'
import MiAgenda   from '@/views/Terapias/MiAgenda.vue'

// Reportes
import Ventas   from '@/views/reportes/Ventas.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [
    { path: '/login'     , name: 'Login'         , component: Login ,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},

    { path: '/'              , name: 'Home'          , component: Home,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/niveles'       , name: 'Niveles'       , component: Niveles,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/usuarios'      , name: 'Usuarios'      , component: Usuarios,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/terapeutas'    , name: 'Terapeutas'    , component: Terapeutas,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/pacientes'     , name: 'Pacientes'     , component: Pacientes,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/prospectos'    , name: 'Prospectos'    , component: Prospectos,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},
      
    { path: '/coordinaciones', name: 'Coordinaciones', component: Coordinaciones,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/confighorario' , name: 'ConfigHorario' , component: ConfigHorario,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/servicios'     , name: 'Servicios'     , component: Servicios,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    // ESCUELA
    { path: '/maestros' , name: 'Maestros' , component: Maestros,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    { path: '/tutores'  , name: 'Tutores'  , component: Tutores,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},
    
    { path: '/alumnos'  , name: 'Alumnos'  , component: Alumnos,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},
    
    { path: '/grupos'   , name: 'Grupos'   , component: Grupos,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},

    // TERPIAS
    { path: '/agenda'     , name: 'Agenda'     , component: Agenda,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},
    { path: '/miagenda'   , name: 'MiAgenda'   , component: MiAgenda,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},


    // REPORTES
    { path: '/ventas'   , name: 'Ventas'   , component: Ventas,
      meta: { ADMIN: true, USUARIO: true, NORMAL: true }},
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)) {
    next()
  } else if (store.state.UsuarioHope.datosUsuarioHope) {
    if (to.matched.some(record => record.meta.USUARIO)) {
      next()
    }
  } else {
    next({
      name: 'Login'
    })
  }
})

export default router