import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';

export default {
  namespaced: true,
  state: {
    loginHope: false,
    datosUsuarioHope: '',
  },

  mutations: {
    LOGEADO(state, value) {
      state.loginHope = value
    },
    DATOS_USUARIO(state, datosUsuarioHope) {
      state.datosUsuarioHope = datosUsuarioHope
    },

    SALIR(state) {
      state.loginHope = false
      state.datosUsuarioHope = ''
    }
  },

  actions: {
    guardarInfo({ commit, dispatch }, usuario) {
      commit('DATOS_USUARIO', usuario)
      commit('LOGEADO', true)
    },

    salirLogin({ commit }) {
      commit('SALIR')
    },
  },

  getters: {
    getLogeadoHope(state) {
      return state.loginHope
    },

    getUsuarioHope(state) {
      return state.datosUsuarioHope
    },
  }
}