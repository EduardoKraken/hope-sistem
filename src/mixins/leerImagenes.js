import Vue from 'vue'
import { mapGetters,mapActions } from 'vuex';

export default {
	methods: {
    fixBinary (bin) {
      var length = bin.length;
      var buf = new ArrayBuffer(length);
      var arr = new Uint8Array(buf);
      for (var i = 0; i < length; i++) {
        arr[i] = bin.charCodeAt(i);
      }
      return buf;
    },

    cargarArchivo(){
      if(!this.archivos){ return }
      //mandamos a ocvertir la imagen a base64 pero mandamos el docuemnto, el formdata, el nombre
      this.procesarImagen2( )
      this.getBase64(this.archivos)
    },

    getBase64(file) {
      var me = this
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        me.vistaPrevia = {
          url: reader.result, 
          image_name: file.name
        }
      };
    },

    onChange(e) {
      var files = e.target.files || e.dataTransfer.files;
      
      if (!files.length) {
        this.dragging = false;
        return;
      }
      
      this.createFile(files[0]);
    },

    createFile(file) {
      
      if (!file.type.match('image.*')) {
        alert('please select txt file');
        this.dragging = false;
        return;
      }
      
      if (file.size > 10000000) {
        alert('please check file size no over 5 MB.')
        this.dragging = false;
        return;
      }
      
      this.file = file;
      this.archivos = file;
      this.getBase64(this.archivos)
      this.dragging = false;
    },

    removeFile() {
      this.vistaPrevia        = null
      this.archivos           = null
      this.file               = '';
    },

  }
}